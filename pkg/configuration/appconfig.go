package configuration

type ConfigurationApp struct {
	App
	AuroraDb
}

// type App struct {
// 	ListenPort            string `envconfig:"UCP_LISTEN_PORT" split_words:"true" default:":9685"`
// 	RootURL               string `envconfig:"UCP_ROOT_PORT" split_words:"true" default:"/utility-cancel-pickup"`
// 	OriginHost            string `envconfig:"UCP_ORIGIN_HOST" split_words:"true" default:"utility-cancel-pickup"`
// 	AppName               string `envconfig:"UCP_APP_NAME" split_words:"true" default:"utility-cancel-pickup"`
// 	Timeout               int64  `envconfig:"UCP_TIMEOUT" split_words:"true" default:"60000"`
// 	Interval              int64  `envconfig:"UCP_INTERVAL" split_words:"true" default:"5"`
// 	JneCancelOrderUrl     string `envconfig:"UCP_JNE_CANCEL_URL" split_words:"true" default:"http://13.214.58.215:9682/jne/cancel"`
// 	SicepatCancelOrderUrl string `envconfig:"UCP_SICEPAT_CANCEL_URL" split_words:"true" default:"http://13.214.58.215:9688/sicepat/cancel"`
// 	SapCancelOrderUrl     string `envconfig:"UCP_SAP_CANCEL_URL" split_words:"true" default:"http://13.214.58.215:9689/sap/cancel"`
// 	NinjaCancelOrderUrl   string `envconfig:"UCP_NINJA_CANCEL_URL" split_words:"true" default:"http://13.214.58.215:9693/ninja/cancel"`
// 	TimeAtFrom            string `envconfig:"UCP_TIME_AT_FROM" split_words:"true" default:"00:01"`
// 	TimeAtTo              string `envconfig:"UCP_TIME_AT_TO" split_words:"true" default:"15:00"`
// 	IdExpressCancelOrderUrl string `envconfig:"UCP_ID_EXPRESS_CANCEL_URL" split_words:"true" default:"http://13.214.58.215:9701/idexpress/cancel"`
// }

// type AuroraDb struct {
// 	Addr     string `envconfig:"UCP_AURORA_ADDRESS" split_words:"true" default:"aurora-db-dev.cluster-ci18lzbpckdr.ap-southeast-1.rds.amazonaws.com"`
// 	Port     int    `envconfig:"UCP_AURORA_PORT" split_words:"true" default:"3306"`
// 	User     string `envconfig:"UCP_AURORA_USER" split_words:"true" default:"admin"`
// 	Password string `envconfig:"UCP_AURORA_PASSWORD" split_words:"true" default:"ihHw7qVqYGpzOGE7qRU8"`
// 	Database string `envconfig:"UCP_AURORA_DATABASE" split_words:"true" default:"solusi_kirim"`
// }

type App struct {
	ListenPort              string `envconfig:"UCP_LISTEN_PORT" split_words:"true" default:""`
	RootURL                 string `envconfig:"UCP_ROOT_PORT" split_words:"true" default:""`
	OriginHost              string `envconfig:"UCP_ORIGIN_HOST" split_words:"true" default:""`
	AppName                 string `envconfig:"UCP_APP_NAME" split_words:"true" default:""`
	Timeout                 int64  `envconfig:"UCP_TIMEOUT" split_words:"true" default:""`
	Interval                int64  `envconfig:"UCP_INTERVAL" split_words:"true" default:""`
	JneCancelOrderUrl       string `envconfig:"UCP_JNE_CANCEL_URL" split_words:"true" default:""`
	SicepatCancelOrderUrl   string `envconfig:"UCP_SICEPAT_CANCEL_URL" split_words:"true" default:""`
	SapCancelOrderUrl       string `envconfig:"UCP_SAP_CANCEL_URL" split_words:"true" default:""`
	NinjaCancelOrderUrl     string `envconfig:"UCP_NINJA_CANCEL_URL" split_words:"true" default:""`
	TimeAtFrom              string `envconfig:"UCP_TIME_AT_FROM" split_words:"true" default:""`
	TimeAtTo                string `envconfig:"UCP_TIME_AT_TO" split_words:"true" default:""`
	IdExpressCancelOrderUrl string `envconfig:"UCP_ID_EXPRESS_CANCEL_URL" split_words:"true" default:""`
}

type AuroraDb struct {
	Addr     string `envconfig:"UCP_AURORA_ADDRESS" split_words:"true" default:""`
	Port     int    `envconfig:"UCP_AURORA_PORT" split_words:"true" default:""`
	User     string `envconfig:"UCP_AURORA_USER" split_words:"true" default:""`
	Password string `envconfig:"UCP_AURORA_PASSWORD" split_words:"true" default:""`
	Database string `envconfig:"UCP_AURORA_DATABASE" split_words:"true" default:""`
}
