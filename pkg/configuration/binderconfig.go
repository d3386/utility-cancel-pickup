package configuration

import (
	"os"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

type ServicesApp struct {
	FullName string
	Config   ConfigurationApp
	Path     string
}

func (s *ServicesApp) Load() {
	if err := envconfig.Process(s.FullName, &s.Config); err != nil {
		log.Error(err)
		os.Exit(1)
	}

	log.Info("Loaded configs: ", s.Config)
	log.Info("Root Path: ", s.Path)
}

func (s *ServicesApp) GetRoot() string {
	return s.Config.RootURL
}

func (s *ServicesApp) GetAuroraDb() AuroraDb {
	return s.Config.AuroraDb
}

func (s *ServicesApp) GetTimeout() int64 {
	return s.Config.Timeout
}

func (s *ServicesApp) JneCancelOrderUrl() string {
	return s.Config.JneCancelOrderUrl
}

func (s *ServicesApp) SicepatCancelOrderUrl() string {
	return s.Config.SicepatCancelOrderUrl
}

func (s *ServicesApp) SapCancelOrderUrl() string {
	return s.Config.SapCancelOrderUrl
}

func (s *ServicesApp) NinjaCancelOrderUrl() string {
	return s.Config.NinjaCancelOrderUrl
}

func (s *ServicesApp) TimeAtFrom() string {
	return s.Config.TimeAtFrom
}

func (s *ServicesApp) TimeAtTo() string {
	return s.Config.TimeAtTo
}

func (s *ServicesApp) IdExpressCancelOrderUrl() string {
	return s.Config.IdExpressCancelOrderUrl
}