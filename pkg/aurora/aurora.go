package aurora

import (
	"database/sql"
	"fmt"
	"sync"

	_ "github.com/go-sql-driver/mysql"
	cm "gitlab.com/d3386/utility-cancel-pickup/pkg/configuration"
)

var (
	dbSql      *sql.DB
	onceAurora sync.Once
)

type AuroraOptions cm.AuroraDb

func NewSingleton(options AuroraOptions) *sql.DB {
	var err error
	auroradbconn := fmt.Sprintf("%s:%s@(%s:%v)/%s", options.User, options.Password, options.Addr, options.Port, options.Database)
	onceAurora.Do(func() {
		dbSql, err = sql.Open("mysql", auroradbconn)
		if err != nil {
			panic(err)
		}
	})

	return dbSql
}
