package aurora

import (
	"gitlab.com/d3386/utility-cancel-pickup/pkg/configuration"
)

func NewOptions(config configuration.ServicesApp) AuroraOptions {
	options := AuroraOptions{
		Addr:     config.GetAuroraDb().Addr,
		Port:     config.GetAuroraDb().Port,
		User:     config.GetAuroraDb().User,
		Password: config.GetAuroraDb().Password,
		Database: config.GetAuroraDb().Database,
	}

	return options
}
