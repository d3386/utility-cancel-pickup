package log

import (
	"time"

	"github.com/sirupsen/logrus"
)

func HandlerStartLogging(handlerName string, logger *logrus.Entry, request interface{}) {
	logger.WithField("request", request).Info(handlerName + " begins")
}

func HandlerEndLogging(handlerName string, logger *logrus.Entry, begin time.Time) {
	elapsed := float64(time.Since(begin).Nanoseconds()) / float64(1e6)
	logger.WithField("elapsed", elapsed).Info(handlerName + " ends")
}
