package request

import (
	"crypto/tls"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"time"

	"github.com/sirupsen/logrus"
)

type request struct {
	Logger           *logrus.Entry
	URI              string
	Method           string
	AdditionalHeader map[string]string
	TimeOut          int64
}

func NewRequest(logger *logrus.Entry, method string, uri string, additionalHeader map[string]string, timeOut int64) request {
	return request{
		Logger:           logger,
		URI:              uri,
		Method:           method,
		AdditionalHeader: additionalHeader,
		TimeOut:          timeOut,
	}
}

func (r request) Send(contentType string, body io.Reader) ([]byte, error) {
	reqs, e := http.NewRequest(r.Method, r.URI, body)
	if e != nil {
		return nil, e
	}

	reqs.Header.Set("Content-Type", contentType)
	for key, val := range r.AdditionalHeader {
		reqs.Header.Set(key, val)
	}

	requestDump, e := httputil.DumpRequest(reqs, true)
	if e != nil {
		r.Logger.WithField("error", e).Error("Catch error")
		return nil, e
	}

	r.Logger.WithFields(logrus.Fields{
		"request": string(requestDump),
		"url":     r.URI,
	}).Info("Sending HTTP request")

	client := r.getHttpClient()

	resp, e := client.Do(reqs)
	if e != nil {
		reqs.Close = true
		client.CloseIdleConnections()
		r.Logger.WithField("error", e).Error("Catch error: ", e.Error())
		return nil, e
	}

	defer resp.Body.Close()

	responseDump, e := httputil.DumpResponse(resp, true)
	if e != nil {
		return nil, e
	}
	r.Logger.WithField("response", string(responseDump)).Info("Receiving HTTP response")

	respBody, e := ioutil.ReadAll(resp.Body)
	if e != nil {
		r.Logger.WithField("error", e).Error("Catch error caught while reading response body")
		return nil, e
	}

	return respBody, nil
}

func (r request) getHttpClient() *http.Client {
	var transCfg *http.Transport

	transCfg = &http.Transport{
		MaxIdleConns:        5,
		MaxConnsPerHost:     100,
		MaxIdleConnsPerHost: 10,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	client := &http.Client{
		Timeout:   time.Second * time.Duration(r.TimeOut),
		Transport: transCfg,
	}
	return client
}
