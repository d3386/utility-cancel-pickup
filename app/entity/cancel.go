package entity

type Test struct {
	Name      string `json:"name,omitempty"`
	CreatedBy string `json:"created_by,omitempty"`
}

type DataOrder struct {
	ID            int64  `json:"id,omitempty"`
	ReceiptNo     string `json:"receipt_no,omitempty"`
	IsCod         int    `json:"is_code,omitempty"`
	TotalPrice    int64  `json:"total_price,omitempty"`
	CourierID     int64  `json:"courier_id,omitempty"`
	Name          string `json:"name,omitempty"`
	UserContactID int64  `json:"user_contact_id,omitempty"`
	IsInvoice     int    `json:"is_invoice,omitempty"`
	Status        int    `json:"status,omitempty"`
}

type RespCancelOrder struct {
	Status int `json:"status"`
}

type Order struct {
	Data CancelRequest `json:"order"`
}

type CancelRequest struct {
	ReceiptNo string     `json:"receipt_no"`
	PickUp    PickUpName `json:"pickup,omitempty"`
}

type PickUpName struct {
	Name string `json:"name"`
}

type PaymentMethodAccountData struct {
	ID              int     `json:"id,omitempty"`
	PaymentMethodID int     `json:"payment_method_id,omitempty"`
	AccountNo       string  `json:"account_no,omitempty"`
	Balance         float64 `json:"balance,omitempty"`
}

type Receiver struct {
	Amount      int64  `json:"amount"`
	Type        int    `json:"type"`
	InvoiceNo   string `json:"invoice_no"`
	PrevBalance int64  `json:"previous_balance"`
	LastBalance int64  `json:"last_balance"`
	AccountID   int    `json:"account_id"`
	Status      int    `json:"status"`
	CreatedBy   string `json:"created_by"`
	CourierID   int64  `json:"courier_id"`
}
