package usecase

type IUsecaseCancelPickup interface {
	Cancel() (e error)
}
