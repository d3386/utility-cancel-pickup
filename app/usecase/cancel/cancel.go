package cancel

import (
	"database/sql"
	"strconv"
	"strings"
	"time"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	sdk "gitlab.com/d3386/library"

	"gitlab.com/d3386/utility-cancel-pickup/app/dto"
	"gitlab.com/d3386/utility-cancel-pickup/app/entity"
	"gitlab.com/d3386/utility-cancel-pickup/app/repositories"
	"gitlab.com/d3386/utility-cancel-pickup/app/repositories/cancel/db"
	"gitlab.com/d3386/utility-cancel-pickup/app/repositories/cancel/http"
	"gitlab.com/d3386/utility-cancel-pickup/app/usecase"
	"gitlab.com/d3386/utility-cancel-pickup/pkg/configuration"
)

type event struct {
	RepoCancelDb   repositories.IRepoCancelDb
	RepoCancelHttp repositories.IRepoCancelHttp
	Logger         *log.Entry
	config         configuration.ServicesApp
	auroraDb       *sql.DB
}

func New(logger *log.Entry, config configuration.ServicesApp, auroraDb *sql.DB) usecase.IUsecaseCancelPickup {
	return &event{
		RepoCancelDb:   db.New(logger, auroraDb),
		RepoCancelHttp: http.New(logger),
		config:         config,
		Logger:         logger,
		auroraDb:       auroraDb,
	}
}

func (c *event) Cancel() (e error) {

	checkTime := checkTime(c.config.TimeAtFrom(), c.config.TimeAtTo())

	if checkTime {
		var data []entity.DataOrder
		data, e = c.RepoCancelDb.GetOrder()
		if e != nil {
			return
		}

		// skip process if data is empty
		if len(data) == 0 {
			return
		}
		for _, value := range data {
			if value.ReceiptNo != "" {
				switch value.CourierID {
				case 1: // JNE
					e = c.CancelPickUpJne(value)
					if e != nil {
						continue
					}
				case 2: // SICEPAT
					e = c.CancelPickUpSicepat(value)
					if e != nil {
						continue
					}
				case 3: // SAP
					e = c.CancelPickUpSap(value)
					if e != nil {
						continue
					}
				case 4: // NINJA
					e = c.CancelPickUpNinja(value)
					if e != nil {
						continue
					}
				case 5: //IDEXPRESS
				e = c.CancelPickUpIdExpress(value)
				if e != nil {
					continue
				}
				}
			}
		}
	}

	return
}

func (c *event) CancelPickUpJne(data entity.DataOrder) (e error) {
	requestCancel := dto.SetCancelRequest(data)
	if data.Status == sdk.ERR_REQUEST_CANCEL_CREATE { // status 119
		// Non COD
		if data.IsCod == 0 {
			// check non cod invoice/ non cod standard
			if data.IsInvoice == 0 {
				// process non cod
				e = c.CancelPaymentNonCod(data)
				if e != nil {
					return
				}
			}
		}

		if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
			return
		}
	} else if data.Status == sdk.ERR_REQUEST_CANCEL {
		var res entity.RespCancelOrder
		if res, e = c.RepoCancelHttp.CancelOrder(c.config.JneCancelOrderUrl(), c.config.GetTimeout(), requestCancel); e != nil {
			return
		}

		if res.Status == sdk.ERR_SUCCESS {
			// Non COD
			if data.IsCod == 0 {
				if data.IsInvoice == 0 {
					// process non cod
					e = c.CancelPaymentNonCod(data)
					if e != nil {
						return
					}
				}
			}
			if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
				return
			}
		}
		return
	} else {
		return
	}

	return
}

func (c *event) CancelPickUpSicepat(data entity.DataOrder) (e error) {
	requestCancel := dto.SetCancelRequest(data)

	if data.Status == sdk.ERR_REQUEST_CANCEL_CREATE { // status 119
		// Non COD
		if data.IsCod == 0 {
			// check non cod invoice/ non cod standard
			if data.IsInvoice == 0 {
				// process non cod
				e = c.CancelPaymentNonCod(data)
				if e != nil {
					return
				}
			}
		}

		if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
			return
		}
	} else if data.Status == sdk.ERR_REQUEST_CANCEL {
		var res entity.RespCancelOrder
		if res, e = c.RepoCancelHttp.CancelOrder(c.config.SicepatCancelOrderUrl(), c.config.GetTimeout(), requestCancel); e != nil {
			return
		}

		if res.Status == sdk.ERR_SUCCESS {
			// Non COD
			if data.IsCod == 0 {
				if data.IsInvoice == 0 {
					// process non cod
					e = c.CancelPaymentNonCod(data)
					if e != nil {
						return
					}
				}
			}

			if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
				return
			}
		}
	} else {
		return
	}

	return
}

func (c *event) CancelPickUpSap(data entity.DataOrder) (e error) {
	requestCancel := dto.SetCancelRequest(data)

	// status 119
	if data.Status == sdk.ERR_REQUEST_CANCEL_CREATE { // status 119
		// Non COD
		if data.IsCod == 0 {
			if data.IsInvoice == 0 {
				// process non cod
				e = c.CancelPaymentNonCod(data)
				if e != nil {
					return
				}
			}
		}

		if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
			return
		}
	} else if data.Status == sdk.ERR_REQUEST_CANCEL {
		var res entity.RespCancelOrder
		if res, e = c.RepoCancelHttp.CancelOrder(c.config.SapCancelOrderUrl(), c.config.GetTimeout(), requestCancel); e != nil {
			return
		}

		if res.Status == sdk.ERR_SUCCESS {
			// Non COD
			if data.IsCod == 0 {
				if data.IsInvoice == 0 {
					// process non cod
					e = c.CancelPaymentNonCod(data)
					if e != nil {
						return
					}
				}
			}

			if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
				return
			}
		}
	} else {
		return
	}

	return
}

func (c *event) CancelPaymentNonCod(data entity.DataOrder) (e error) {
	var account entity.PaymentMethodAccountData

	if account, e = c.RepoCancelDb.GetPaymentMethodAccount(data.UserContactID); e != nil {
		return
	}

	var prevBalance int64
	var lastBalance int64
	userContactID := strconv.FormatInt(data.UserContactID, 10)
	if lastBalance, prevBalance, e = c.RepoCancelDb.UpdatePayment(userContactID, &data.TotalPrice); e != nil {
		return
	}

	receiver := SetDataReceiver(userContactID, data.TotalPrice, prevBalance, lastBalance, account.ID, data.CourierID)
	if e = c.RepoCancelDb.InsertPayment(data.ReceiptNo, userContactID, receiver); e != nil {
		return
	}
	return
}

func (c *event) CancelPickUpNinja(data entity.DataOrder) (e error) {
	requestCancel := dto.SetCancelRequest(data)

	// status 119
	if data.Status == sdk.ERR_REQUEST_CANCEL_CREATE { // status 119
		// Non COD
		if data.IsCod == 0 {
			if data.IsInvoice == 0 {
				// process non cod
				e = c.CancelPaymentNonCod(data)
				if e != nil {
					return
				}
			}
		}

		if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
			return
		}
	} else if data.Status == sdk.ERR_REQUEST_CANCEL {
		var res entity.RespCancelOrder
		if res, e = c.RepoCancelHttp.CancelOrder(c.config.NinjaCancelOrderUrl(), c.config.GetTimeout(), requestCancel); e != nil {
			return
		}

		if res.Status == sdk.ERR_SUCCESS {
			// Non COD
			if data.IsCod == 0 {
				if data.IsInvoice == 0 {
					// process non cod
					e = c.CancelPaymentNonCod(data)
					if e != nil {
						return
					}
				}
			}

			if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
				return
			}
		}
	} else {
		return
	}

	return
}

func (c *event) CancelPickUpIdExpress(data entity.DataOrder) (e error) {
	requestCancel := dto.SetCancelRequest(data)

	// status 119
	if data.Status == sdk.ERR_REQUEST_CANCEL_CREATE { // status 119
		// Non COD
		if data.IsCod == 0 {
			if data.IsInvoice == 0 {
				// process non cod
				e = c.CancelPaymentNonCod(data)
				if e != nil {
					return
				}
			}
		}

		if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
			return
		}
	} else if data.Status == sdk.ERR_REQUEST_CANCEL {
		var res entity.RespCancelOrder
		if res, e = c.RepoCancelHttp.CancelOrder(c.config.IdExpressCancelOrderUrl(), c.config.GetTimeout(), requestCancel); e != nil {
			return
		}

		if res.Status == sdk.ERR_SUCCESS {
			// Non COD
			if data.IsCod == 0 {
				if data.IsInvoice == 0 {
					// process non cod
					e = c.CancelPaymentNonCod(data)
					if e != nil {
						return
					}
				}
			}

			if e = c.RepoCancelDb.UpdateStatusOrder(data.ID); e != nil {
				return
			}
		}
	} else {
		return
	}

	return
}

func SetDataReceiver(userContactID string, totalPrice int64, prevBalance int64, lastBalance int64, ID int, courierID int64) (dataReceiver entity.Receiver) {
	invoiceNo := strings.ToUpper("P" + userContactID + "-" + xid.New().String())
	dataReceiver.Amount = totalPrice
	dataReceiver.Type = 3
	dataReceiver.InvoiceNo = invoiceNo
	dataReceiver.PrevBalance = prevBalance
	dataReceiver.LastBalance = lastBalance
	dataReceiver.AccountID = ID
	dataReceiver.Status = 0
	dataReceiver.CreatedBy = "SYSTEM"
	dataReceiver.CourierID = courierID
	return
}

func checkTime(TimeAtFrom string, TimeAtTo string) (checkTime bool) {
	layoutTime := "15:04"
	check, _ := time.Parse(layoutTime, time.Now().Format(layoutTime))
	start, _ := time.Parse(layoutTime, TimeAtFrom)
	end, _ := time.Parse(layoutTime, TimeAtTo)
	checkTime = InTimeSpan(start, end, check)
	return
}

func InTimeSpan(start, end, check time.Time) bool {
	if start.Before(end) {
		return !check.Before(start) && !check.After(end)
	}
	if start.Equal(end) {
		return check.Equal(start)
	}
	return !start.After(check) || !end.Before(check)
}
