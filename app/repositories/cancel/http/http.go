package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/d3386/utility-cancel-pickup/app/entity"
	"gitlab.com/d3386/utility-cancel-pickup/app/repositories"
	"gitlab.com/d3386/utility-cancel-pickup/pkg/request"
)

type event struct {
	Logger *log.Entry
}

func New(logger *log.Entry) repositories.IRepoCancelHttp {
	return &event{
		Logger: logger,
	}
}

func (c *event) CancelOrder(uri string, timeout int64, req entity.Order) (res entity.RespCancelOrder, e error) {
	c.Logger.Info("CancelOrder: Cancel order")

	var buf bytes.Buffer
	e = json.NewEncoder(&buf).Encode(req)
	if e != nil {
		return
	}

	header := map[string]string{}
	contentType := "application/json; charset=utf-8"
	rawResponse, e := request.NewRequest(c.Logger, http.MethodPost, uri, header, timeout).Send(contentType, &buf)
	if e != nil {
		return
	}
	fmt.Println("xxxxxxxxxxx :", string(rawResponse))
	if e = json.Unmarshal(rawResponse, &res); e != nil {
		return
	}

	return
}
