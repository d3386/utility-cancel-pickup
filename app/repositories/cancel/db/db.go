package db

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"

	log "github.com/sirupsen/logrus"
	sdk "gitlab.com/d3386/library"
	"gitlab.com/d3386/utility-cancel-pickup/app/entity"
	"gitlab.com/d3386/utility-cancel-pickup/app/repositories"
)

type event struct {
	Logger   *log.Entry
	AuroraDb *sql.DB
}

func New(logger *log.Entry, auroraDb *sql.DB) repositories.IRepoCancelDb {
	return &event{
		Logger:   logger,
		AuroraDb: auroraDb,
	}
}

func (c *event) Cancel(req entity.Test) (e error) {
	c.Logger.Info("repositories-test: Query create test")
	_, e = c.AuroraDb.Exec(createTest, req.Name, req.CreatedBy)
	if e != nil {
		return
	}

	return
}

func (c *event) GetOrder() (order []entity.DataOrder, e error) {
	c.Logger.Info("repositories-GetOrder: Query get order data")

	var data *sql.Rows
	query := fmt.Sprintf(getOrderData, sdk.ERR_REQUEST_CANCEL, sdk.ERR_REQUEST_CANCEL_CREATE)
	data, e = c.AuroraDb.Query(query)
	if e != nil {
		return
	}

	for data.Next() {
		var dt entity.DataOrder
		e = data.Scan(&dt.ID, &dt.ReceiptNo, &dt.IsCod, &dt.TotalPrice, &dt.CourierID, &dt.Name, &dt.UserContactID, &dt.IsInvoice, &dt.Status)
		if e != nil {
			return
		}
		order = append(order, dt)
	}

	return
}

func (c *event) UpdateStatusOrder(orderID int64) (e error) {
	c.Logger.Info("repositories-UpdateStatusOrder: Query update status order")

	_, e = c.AuroraDb.Exec(updateStatusOrder, sdk.ERR_CANCEL, "CANCEL ORDER/PICKUP", orderID)
	if e != nil {
		return
	}

	return
}

func (c *event) GetPaymentMethodAccount(userContactID int64) (account entity.PaymentMethodAccountData, e error) {
	c.Logger.Info("repositories-GetPaymentMethodAccount: Query get payment method account")
	strUserContactID := strconv.FormatInt(userContactID, 10)

	query := getPaymentMethod
	var condition string
	clause := false

	if !clause {
		condition += " WHERE "
	} else {
		condition += " AND "
	}
	condition += "user_contact_id = '" + strUserContactID + "'"
	clause = true

	query += condition

	c.Logger.Info("repository: GetPaymentMethodAccount-Query")

	result, err := c.AuroraDb.Query(query)
	if err != nil {
		return
	}
	c.Logger.Info("query: ", query)

	defer result.Close()

	for result.Next() {
		if e = result.Scan(&account.ID, &account.PaymentMethodID, &account.AccountNo, &account.Balance); e != nil {
			return
		}
	}

	return
}

func (c *event) UpdatePayment(strUserContactID string, reqAmount *int64) (lastBalance int64, prevBalance int64, e error) {
	c.Logger.Info("repositories-UpdatePayment: Query update payment")
	ctx := context.Background()

	tx, e := c.AuroraDb.BeginTx(ctx, nil)
	if e != nil {
		return
	}

	defer tx.Rollback()

	query := fmt.Sprintf(getBalance, strUserContactID)
	resultBalance, e := c.AuroraDb.Query(query)
	if e != nil {
		return
	}

	for resultBalance.Next() {
		if e = resultBalance.Scan(&prevBalance); e != nil {
			return
		}
	}

	var result sql.Result
	if reqAmount == nil {
		query := fmt.Sprintf(updatePayment, 0, strUserContactID)

		c.Logger.Info("query: ", query)
		result, e = c.AuroraDb.ExecContext(ctx, query)
		if e != nil {
			return
		}
	} else {
		query := fmt.Sprintf(updatePayment, *reqAmount, strUserContactID)

		c.Logger.Info("query: ", query)
		result, e = c.AuroraDb.ExecContext(ctx, query)
		if e != nil {
			return
		}
	}

	_, e = result.RowsAffected()
	if e != nil {
		return
	}

	if e = tx.Commit(); e != nil {
		return
	}

	return prevBalance + *reqAmount, prevBalance, nil
}

func (c *event) InsertPayment(receiptNo, strUserContactID string, params entity.Receiver) (e error) {
	c.Logger.Info("repositories-InsertPayment: Query insert payment")
	ctx := context.Background()
	tx, e := c.AuroraDb.BeginTx(ctx, nil)
	if e != nil {
		return
	}

	defer tx.Rollback()

	query := "INSERT INTO payment(amount, type, invoice_no, previous_balance, last_balance, account_id, status, remark, created_by, courier_id) VALUES "
	data := []interface{}{}
	query += "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?),"
	remark := fmt.Sprintf("REFUND ONGKIR NON COD RESI %s CANCEL ORDER", receiptNo)
	data = append(data, params.Amount, params.Type, params.InvoiceNo, params.PrevBalance, params.LastBalance, params.AccountID, 0, remark, "SYSTEM", params.CourierID)

	query = query[0 : len(query)-1]
	c.Logger.Info("query: ", query)

	result, e := tx.ExecContext(ctx, query, data...)
	if e != nil {
		return
	}
	result.LastInsertId()

	// Commit the transaction.
	if e = tx.Commit(); e != nil {
		return
	}
	return
}
