package db

const (
	createTest string = `INSERT INTO simulate (name, created_by)
	VALUES (?, ?);`

	getOrderData string = "SELECT o.id, o.receipt_no, o.is_cod, o.total_price, o.courier_id, pc.name, u.user_contact_id, is_invoice, " +
		"o.status FROM `order` o JOIN pickup_contact pc ON o.pickup_contact_id = pc.id JOIN user u ON o.user_id = u.id " +
		"WHERE o.status IN( %v, %v)"

	updateStatusOrder string = "UPDATE `order` SET status = ?, remark = ? WHERE id = ?"

	getPaymentMethod string = "SELECT id, payment_method_id, account_no, balance FROM payment_method_account"

	getBalance string = "SELECT balance FROM payment_method_account WHERE user_contact_id = %s FOR UPDATE"

	updatePayment string = "UPDATE payment_method_account SET balance = balance + %v " +
		"WHERE user_contact_id = %s"
)
