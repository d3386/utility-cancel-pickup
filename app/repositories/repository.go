package repositories

import (
	"gitlab.com/d3386/utility-cancel-pickup/app/entity"
)

type IRepoCancelDb interface {
	Cancel(req entity.Test) (e error)
	GetOrder() (order []entity.DataOrder, e error)
	UpdateStatusOrder(orderID int64) (e error)
	GetPaymentMethodAccount(userContactID int64) (account entity.PaymentMethodAccountData, e error)
	UpdatePayment(strUserContactID string, reqAmount *int64) (lastBalance int64, prevBalance int64, e error)
	InsertPayment(receiptNo, strUserContactID string, params entity.Receiver) (e error)
}

type IRepoCancelHttp interface {
	CancelOrder(uri string, timeout int64, req entity.Order) (res entity.RespCancelOrder, e error)
}
