package dto

import (
	"gitlab.com/d3386/utility-cancel-pickup/app/entity"
)

func SetCancelRequest(req entity.DataOrder) (data entity.Order) {
	data.Data.ReceiptNo = req.ReceiptNo
	data.Data.PickUp.Name = req.Name
	return
}
