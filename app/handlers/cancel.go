package handlers

import (
	"database/sql"

	"github.com/jasonlvhit/gocron"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/d3386/utility-cancel-pickup/app/usecase/cancel"
	"gitlab.com/d3386/utility-cancel-pickup/pkg/configuration"
)

type cron struct {
	config   configuration.ServicesApp
	auroraDb *sql.DB
}

func New(config configuration.ServicesApp, auroraDb *sql.DB) *cron {
	return &cron{
		config:   config,
		auroraDb: auroraDb,
	}
}

func (c *cron) ExecCron(config configuration.ServicesApp) (err error) {
	if config.Config.App.Interval > 0 {
		log.Info("Register cron ExecCancel")
		err = gocron.Every(uint64(c.config.Config.Interval)).Seconds().Do(ExecCancelPickup, config, c.auroraDb)
		if err != nil {
			log.Error("err register cron : ", err)
			return
		}
	}

	return nil
}

func ExecCancelPickup(config configuration.ServicesApp, auroraDB *sql.DB) {
	logger := log.WithFields(log.Fields{
		"job":    "ExecCancelPickup",
		"msg_id": xid.New().String(),
	})
	logger.Debug("Running")

	err := cancel.New(logger, config, auroraDB).Cancel()
	if err != nil {
		logger.Error("Catch error:", err.Error())
	}
}
