FROM golang:latest AS builder
RUN apt-get update
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
WORKDIR /go/src/app
COPY . .
RUN go mod init gitlab.com/d3386/utility-cancel-pickup
RUN go mod tidy
RUN go mod edit -require github.com/go-sql-driver/mysql@v1.7.1
RUN go mod tidy
RUN go mod download
run go get gitlab.com/d3386/library@main
RUN go build -o /go/bin/app

FROM alpine:latest
RUN apk add tzdata
ENV TZ Asia/Jakarta
COPY --from=builder /go/bin/app .
ENTRYPOINT ["./app"]
