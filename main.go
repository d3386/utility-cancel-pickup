package main

import (
	"path"
	"path/filepath"
	"runtime"

	"github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
	"gitlab.com/d3386/utility-cancel-pickup/app/handlers"
	"gitlab.com/d3386/utility-cancel-pickup/pkg/aurora"
	"gitlab.com/d3386/utility-cancel-pickup/pkg/configuration"
)

func initLogger() {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05.999",
	})
}

func configAndStartServer() {
	configMain := configuration.ServicesApp{
		FullName: "utility-cancel-pickup",
		Path:     setPath(),
	}

	configMain.Load()

	cron(configMain)
}

func setPath() string {
	_, b, _, _ := runtime.Caller(0)
	d := path.Join(path.Dir(b))
	basePath := filepath.Dir(d)

	return basePath
}

func cron(config configuration.ServicesApp) {
	err := handlers.New(config,
		aurora.NewSingleton(aurora.NewOptions(config))).ExecCron(config)
	if err != nil {
		log.Error("cron utility-cancel-pickup : ", err)
	}

}
func main() {
	initLogger()
	configAndStartServer()

	<-gocron.Start()
}
